package eu.fabiorinnone.faerscompare.utilities;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import difflib.PatchFailedException;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by fabior on 03/02/17.
 */
public class DiffUtil {

    @Nullable
    public static String getDiff(File firstFile, File secondFile, String splitValue) {

        String firstFileStr = "";
        String secondFileStr = "";
        try {
            firstFileStr = FileUtils.readFileToString(firstFile);
            secondFileStr = FileUtils.readFileToString(secondFile);
        } catch (IOException e){
            e.printStackTrace();
        }

        List<String> first = new ArrayList(Arrays.asList(firstFileStr.split(splitValue)));
        List<String> second = new ArrayList(Arrays.asList(secondFileStr.split(splitValue)));

        Patch patch = DiffUtils.diff(first, second);

        for(Delta delta : patch.getDeltas())
            System.out.println(delta);

        try {
            List<String> result = (List<String>) patch.applyTo(first);

            if(!result.equals(second)) {
                System.err.print("The patch.applyTo 'rebuild from diffs' method has not produced a result that ");
                System.err.println("the revised string-list");
                return null;
            }

            StringBuilder stringList = new StringBuilder();

            for(int i = 0; i < result.size(); i++) {
                String s = result.get(i);
                if(i != result.size() - 1)
                    stringList.append(s + splitValue);
                else
                    stringList.append(s);
            }

            String merge = String.valueOf(stringList);

            return merge;
        } catch (PatchFailedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
