package eu.fabiorinnone.faerscompare.utilities;

/**
 * Created by fabior on 02/02/17.
 */
public class Common {

    public static int EXIT_CODE_OK = 0;
    public static int EXIT_CODE_ERR_ARGS = 1;
    public static int EXIT_ORIENTDB_ERR = 2;
    public static int EXIT_CODE_OUT_FILE_ERR = 3;

    public static int TIMEOUT = 60 * 60 * 1000;

    public static String MYSQL_OUTPUT_FILE = "results/mysql_output.csv";
    public static String ORIENTDB_OUTPUT_FILE = "results/orientdb_output.csv";
}
