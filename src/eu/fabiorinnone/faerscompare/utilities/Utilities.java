package eu.fabiorinnone.faerscompare.utilities;

import com.orientechnologies.orient.core.sql.OSQLEngine;
import com.orientechnologies.orient.core.sql.functions.OSQLFunction;
import com.orientechnologies.orient.graph.sql.functions.OGraphFunctionFactory;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by fabior on 02/02/17.
 */
public class Utilities {

    public static void registerGraphFunctions() {
        OGraphFunctionFactory graphFunctions = new OGraphFunctionFactory();
        Set<String> names = graphFunctions.getFunctionNames();

        for (String name : names) {
            System.out.println("ODB graph function found: [" + name + "]");
            OSQLEngine.getInstance().registerFunction(name, graphFunctions.createFunction(name));
            OSQLFunction function = OSQLEngine.getInstance().getFunction(name);
            if (function != null) {
                System.out.println("ODB graph function [" + name + "] is registered: [" + function.getSyntax() + "]");
            }
            else {
                System.out.println("ODB graph function [" + name + "] NOT registered!");
            }
        }
    }

    public static void printHeapMemoryInfo() {
        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
        MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
        long maxMemory = heapUsage.getMax();
        long usedMemory = heapUsage.getUsed();
        System.out.print("Java current heap space is " + usedMemory + "/" + maxMemory);
        BigDecimal totalMemoryMb =
                new BigDecimal(usedMemory / Math.pow(1024, 2)).setScale(2, BigDecimal.ROUND_CEILING);
        BigDecimal maxMemoryMb =
                new BigDecimal(maxMemory / Math.pow(1024, 2)).setScale(2, BigDecimal.ROUND_CEILING);
        System.out.println(" (" + totalMemoryMb + " MB/" + maxMemoryMb + " MB)");
    }

    public static void checkFile(File file) {
        if (!file.exists())
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        else
            file.delete();
    }

    public static boolean isFullyProjection(String query) {
        String[] splitQuery = query.split(" ");
        if (splitQuery[0].equals("select") && splitQuery[1].equals("*"))
            return true;
        if (splitQuery[0].equals("select") && splitQuery[1].endsWith("from"))
            return true;
        return false;
    }
}
