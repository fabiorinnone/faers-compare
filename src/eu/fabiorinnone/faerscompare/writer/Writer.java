package eu.fabiorinnone.faerscompare.writer;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by fabior on 02/02/17.
 */
public interface Writer {

    public void write() throws SQLException, IOException;
}
