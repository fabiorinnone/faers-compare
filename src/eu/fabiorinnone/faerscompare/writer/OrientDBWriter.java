package eu.fabiorinnone.faerscompare.writer;

import com.google.common.collect.Lists;
import com.tinkerpop.blueprints.Vertex;
import eu.fabiorinnone.faerscompare.utilities.Common;
import eu.fabiorinnone.faerscompare.utilities.Utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by fabior on 02/02/17.
 */
public class OrientDBWriter extends AbstractWriter {

    Iterable<Vertex> vertices;

    public OrientDBWriter(Iterable<Vertex> vertices) throws IOException {
        this.vertices = vertices;

        outputFile = new File(Common.ORIENTDB_OUTPUT_FILE);
        Utilities.checkFile(outputFile);

        writer = new BufferedWriter(new FileWriter(outputFile.getAbsoluteFile(), true));
    }

    @Override
    public void write() throws IOException {
        write(false);
    }

    public void write(boolean fullyProjection) throws IOException {
        Vertex firstVertex = vertices.iterator().next();
        Set<String> keys = firstVertex.getPropertyKeys();
        List<String> keysList = new ArrayList<String>();
        keysList.addAll(keys);

        if (fullyProjection)
            keysList = Lists.reverse(keysList);

        int keysCount = keysList.size();

        int i = 0;
        String line = "";
        for (String key : keysList) {
            line += "\"" + key + "\"";

            if (i++ < keysCount -1)
                line += ",";
        }
        writeLine(line);

        i = 0;
        line = "";
        for (String key : keysList) { //workaround issue #5
            line += "\"" + firstVertex.getProperty(key) + "\"";

            if (i++ < keysCount - 1)
                line += ",";
        }
        writeLine(line);

        for (Vertex vertex : vertices) {
            i = 0;
            line = "";
            for (String key : keysList) {
                line += "\"" + vertex.getProperty(key) + "\"";

                if (i++ < keysCount - 1)
                    line += ",";
            }
            writeLine(line);
        }

        writer.close();
    }
}
