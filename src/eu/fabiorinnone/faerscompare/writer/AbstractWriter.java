package eu.fabiorinnone.faerscompare.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by fabior on 02/02/17.
 */
public abstract class AbstractWriter implements Writer {

    protected File outputFile;
    protected BufferedWriter writer;

    @Override
    public abstract void write() throws SQLException, IOException;

    public void writeLine(String line) throws IOException {
        StringBuffer sb = new StringBuffer(line);

        synchronized(this) {
            writer.write(sb.toString() + "\n");
        }
    }
}
