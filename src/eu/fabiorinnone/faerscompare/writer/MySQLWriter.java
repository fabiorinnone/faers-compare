package eu.fabiorinnone.faerscompare.writer;

import eu.fabiorinnone.faerscompare.utilities.Common;
import eu.fabiorinnone.faerscompare.utilities.Utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Created by fabior on 02/02/17.
 */
public class MySQLWriter extends AbstractWriter {

    ResultSet resultSet;

    public MySQLWriter(ResultSet resultSet) throws IOException {
        this.resultSet = resultSet;

        outputFile = new File(Common.MYSQL_OUTPUT_FILE);
        Utilities.checkFile(outputFile);

        writer = new BufferedWriter(new FileWriter(outputFile.getAbsoluteFile(), true));
    }

    @Override
    public void write() throws SQLException, IOException {
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();

        String line = "";
        for (int i = 1; i <= columnCount; i++) {
            String columnName = resultSetMetaData.getColumnName(i);
            line += "\"" + columnName + "\"";

            if (i < columnCount)
                line += ",";
        }
        writeLine(line);

        while (resultSet.next()) {
            line = "";
            for (int i = 1; i <= columnCount; i++) {
                line += "\""+ resultSet.getString(i) + "\"";

                if (i < columnCount)
                    line += ",";
            }
            writeLine(line);
        }

        writer.close();
    }
}
