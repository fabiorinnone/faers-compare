package eu.fabiorinnone.faerscompare.main;

import com.orientechnologies.orient.core.exception.OStorageException;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

import eu.fabiorinnone.faerscompare.database.JDBCUtilities;
import eu.fabiorinnone.faerscompare.database.MySQLAdapter;
import eu.fabiorinnone.faerscompare.database.OrientDBAdapter;
import eu.fabiorinnone.faerscompare.database.OrientDBUtilities;
import eu.fabiorinnone.faerscompare.utilities.Common;
import eu.fabiorinnone.faerscompare.utilities.DiffUtil;
import eu.fabiorinnone.faerscompare.utilities.Utilities;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Main {

    private static final String BUNDLE = "eu.fabiorinnone.faerscompare.main.resources.database";

    private static ResourceBundle bundle = null;

    public static void main(String[] args) {
        if (args.length != 1 && args.length != 2) {
            System.out.println("Usage:\njava -jar faers-compare.jar "
                    + "\"mysql_query\" \"orientdb_query\"");
            System.out.println("java -jar faers-compare.jar \"query\"");
            System.exit(Common.EXIT_CODE_ERR_ARGS);
        }

        String mysqlQuery = "";
        String orientdbQuery = "";

        if (args.length == 1)
            mysqlQuery = orientdbQuery = args[0];
        else {
            mysqlQuery = args[0];
            orientdbQuery = args[1];
        }

        System.out.println("Initialization...");
        Utilities.registerGraphFunctions();
        System.out.println();

        System.out.println("Estabilishing connection to MySQL...");

        HashMap<String,String> mysqlProperties = new HashMap<String,String>();
        HashMap<String,String> orientdbProperties = new HashMap<String,String>();

        mysqlProperties.put("mysql.dbms", getString("mysql.dbms"));
        mysqlProperties.put("mysql.driver", getString("mysql.driver"));
        mysqlProperties.put("mysql.database_name", getString("mysql.database_name"));
        mysqlProperties.put("mysql.user_name", getString("mysql.user_name"));
        mysqlProperties.put("mysql.password", getString("mysql.password"));
        mysqlProperties.put("mysql.server_name", getString("mysql.server_name"));
        mysqlProperties.put("mysql.port_number", getString("mysql.port_number"));

        orientdbProperties.put("orientdb.dbms", getString("orientdb.dbms"));
        orientdbProperties.put("orientdb.database_name", getString("orientdb.database_name"));
        orientdbProperties.put("orientdb.user_name", getString("orientdb.user_name"));
        orientdbProperties.put("orientdb.password", getString("orientdb.password"));
        orientdbProperties.put("orientdb.database_path", getString("orientdb.database_path"));
        orientdbProperties.put("orientdb.engine", getString("orientdb.engine"));

        File resultsDir = new File("results/");
        if (!resultsDir.exists())
            resultsDir.mkdir();
        
        JDBCUtilities jdbcUtilities = null;
        Connection mySQLConnection = null;
        try {
            jdbcUtilities = new JDBCUtilities(mysqlProperties);
        } catch (IOException e) {
            System.err.println("Problem reading properties file");
            e.printStackTrace();
        }

        System.out.println("\nEstabilishing connection to OrientDB...");

        OrientDBUtilities orientDBUtilities = null;
        OrientGraph graph = null;
        try {
            orientDBUtilities = new OrientDBUtilities(orientdbProperties);
        } catch (IOException e) {
            System.err.println("Problem reading properties file");
            e.printStackTrace();
        }

        OrientDBAdapter orientDBAdapter = null;
        try {
            graph = orientDBUtilities.getTxGraph();
            orientDBAdapter = OrientDBAdapter.getInstance(graph);
        } catch (OStorageException e) {
            System.out.println("Error occourred: " + e.getMessage());
            System.out.println("Exiting...");
            System.exit(Common.EXIT_ORIENTDB_ERR);
        }

        try {
            mySQLConnection = jdbcUtilities.getConnection();
            MySQLAdapter mySQLAdapter = MySQLAdapter.getInstance(mySQLConnection, jdbcUtilities.dbName,
                jdbcUtilities.dbms);

            System.out.println("Executing MySQL query:\n'" + mysqlQuery + "'");
            mySQLAdapter.executeQuery(mysqlQuery);

            String orientdbQueryWithTimeout = OrientDBUtilities.addTimeoutToQuery(orientdbQuery);
            System.out.println("Executing OrientDB query:\n'" + orientdbQuery + "'");
            try {
                orientDBAdapter.executeQuery(orientdbQueryWithTimeout);
            }
            catch (Exception e) {
                System.err.println("An error occourred: " + e.getMessage());
            }
        } catch (SQLException e) {
            JDBCUtilities.printSQLException(e);
        } finally {
            JDBCUtilities.closeConnection(mySQLConnection);
        }

        //diff files
        File mysqlOutputFile = new File(Common.MYSQL_OUTPUT_FILE);
        File orientdbOutputFile = new File(Common.ORIENTDB_OUTPUT_FILE);

        if (!mysqlOutputFile.exists() || !orientdbOutputFile.exists()) {
            System.err.println("An error occourred");
            System.exit(Common.EXIT_CODE_OUT_FILE_ERR);
        }

        System.out.println("\nShow output files differences:");
        DiffUtil.getDiff(mysqlOutputFile, orientdbOutputFile, "\n");

        System.out.println("\nExiting...");

        System.exit(Common.EXIT_CODE_OK);
    }

    public static ResourceBundle getResourceBundle() {
        if(bundle == null) {
            bundle = ResourceBundle.getBundle(BUNDLE);
        }
        return bundle;
    }

    public static String getString(String key) {
        String value = null;
        try {
            value = getResourceBundle().getString(key);
        }
        catch(MissingResourceException e) {
            System.out.println("java.util.MissingResourceException: Couldn't find value for: " + key);
        }
        if(value == null) {
            value = "Could not find resource: " + key;
        }
        return value;
    }
}
