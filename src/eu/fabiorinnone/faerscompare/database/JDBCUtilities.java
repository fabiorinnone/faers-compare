package eu.fabiorinnone.faerscompare.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by fabior on 02/02/17.
 */
public class JDBCUtilities {

    public String dbms;
    public String jarFile;
    public String dbName;
    public String userName;
    public String password;
    public String urlString;

    private String driver;
    private String serverName;
    private int portNumber;
    //private Properties prop;

    public JDBCUtilities(HashMap<String,String> properties) throws IOException {
        super();
        this.setProperties(properties);
    }

    private void setProperties(HashMap<String,String> properties) throws IOException {
        //this.prop = new Properties();
        //FileInputStream fis = new FileInputStream(fileName);
        //prop.loadFromXML(fis);

        this.dbms = properties.get("mysql.dbms");
        this.jarFile = properties.get("mysql.jar_file");
        this.driver = properties.get("mysql.driver");
        this.dbName = properties.get("mysql.database_name");
        this.userName = properties.get("mysql.user_name");
        this.password = properties.get("mysql.password");
        this.serverName = properties.get("mysql.server_name");
        this.portNumber = Integer.parseInt(properties.get("mysql.port_number"));

        System.out.println("Set the following properties:");
        System.out.println("dbms: " + dbms);
        System.out.println("driver: " + driver);
        System.out.println("dbName: " + dbName);
        System.out.println("userName: " + userName);
        System.out.println("serverName: " + serverName);
        System.out.println("portNumber: " + portNumber);
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);

        String currentUrlString = null;

        if (this.dbms.equals("mysql")) {
            currentUrlString = "jdbc:" + this.dbms + "://" + this.serverName +
                    ":" + this.portNumber + "/";
            conn =
                    DriverManager.getConnection(currentUrlString,
                            connectionProps);

            this.urlString = currentUrlString + this.dbName;
            conn.setCatalog(this.dbName);
        } else if (this.dbms.equals("derby")) {
            this.urlString = "jdbc:" + this.dbms + ":" + this.dbName;

            conn =
                    DriverManager.getConnection(this.urlString +
                            ";create=true", connectionProps);

        }
        //System.out.println("Connected to database...\n");
        return conn;
    }
    public static boolean ignoreSQLException(String sqlState) {
        if (sqlState == null) {
            System.out.println("The SQL state is not defined!");
            return false;
        }
        // X0Y32: Jar file already exists in schema
        if (sqlState.equalsIgnoreCase("X0Y32"))
            return true;
        // 42Y55: Table already exists in schema
        if (sqlState.equalsIgnoreCase("42Y55"))
            return true;
        return false;
    }

    public static void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                if (ignoreSQLException(((SQLException)e).getSQLState()) == false) {
                    e.printStackTrace(System.err);
                    System.err.println("SQLState: " + ((SQLException)e).getSQLState());
                    System.err.println("Error Code: " + ((SQLException)e).getErrorCode());
                    System.err.println("Message: " + e.getMessage());
                    Throwable t = ex.getCause();
                    while (t != null) {
                        System.out.println("Cause: " + t);
                        t = t.getCause();
                    }
                }
            }
        }
    }

    public static void closeConnection(Connection connArg) {
        System.out.println("Releasing all open resources...");
        try {
            if (connArg != null) {
                connArg.close();
                connArg = null;
            }
        } catch (SQLException sqle) {
            printSQLException(sqle);
        }
    }
}
