package eu.fabiorinnone.faerscompare.database;

import eu.fabiorinnone.faerscompare.writer.MySQLWriter;
import eu.fabiorinnone.faerscompare.writer.Writer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by fabior on 02/02/17.
 */
public class MySQLAdapter {

    private static MySQLAdapter instance = null;

    private String dbName;
    private Connection con;
    private String dbms;

    private MySQLAdapter(Connection connArg, String dbNameArg, String dbmsArg) {
        super();
        this.con = connArg;
        this.dbName = dbNameArg;
        this.dbms = dbmsArg;
    }

    public static synchronized MySQLAdapter getInstance(Connection connArg,
                                                             String dbNameArg,
                                                             String dbmsArg) {
        if (instance == null) {
            instance = new MySQLAdapter(connArg, dbNameArg, dbmsArg);
        }
        return instance;
    }

    public void executeQuery(String query) throws SQLException {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            if (rs != null) {
                Writer writer = new MySQLWriter(rs);
                writer.write();
            }
        } catch (SQLException e) {
            JDBCUtilities.printSQLException(e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
}
