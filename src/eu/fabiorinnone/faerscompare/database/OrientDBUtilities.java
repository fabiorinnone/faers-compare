package eu.fabiorinnone.faerscompare.database;

import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import eu.fabiorinnone.faerscompare.utilities.Common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;

/**
 * Created by fabior on 02/02/17.
 */
public class OrientDBUtilities {

    private final OrientGraphFactory factory;

    public String dbms;
    public String dbName;
    public String userName;
    public String password;
    public String databasePath;
    public String engine;

    //private Properties prop;

    public OrientDBUtilities(HashMap<String,String> properties) throws IOException {
        super();
        this.setProperties(properties);

        String database = engine + ":" + databasePath + "/" + dbName;
        factory = new OrientGraphFactory(database, userName, password).setupPool(1, 10);
    }

    private void setProperties(HashMap<String,String> properties) throws FileNotFoundException,
            IOException,
            InvalidPropertiesFormatException {
        //this.prop = new Properties();
        //FileInputStream fis = new FileInputStream(fileName);
        //prop.loadFromXML(fis);

        this.dbms = properties.get("orientdb.dbms");
        this.dbName = properties.get("orientdb.database_name");
        this.userName = properties.get("orientdb.user_name");
        this.password = properties.get("orientdb.password");
        this.databasePath = properties.get("orientdb.database_path");
        this.engine = properties.get("orientdb.engine");

        System.out.println("Set the following properties:");
        System.out.println("dbms: " + dbms);
        System.out.println("dbName: " + dbName);
        System.out.println("userName: " + userName);
        System.out.println("databasePath: " + databasePath);
        System.out.println("engine: " + engine);
    }

    public static String addTimeoutToQuery(String query) {
        String queryWithTimeout = "";

        if (!query.contains("parallel"))
            queryWithTimeout = query + " timeout " + Common.TIMEOUT;
        else {
            String[] splittedQuery = query.split("parallel");
            queryWithTimeout = splittedQuery[0] + " timeout " + Common.TIMEOUT + " parallel";
        }

        return queryWithTimeout;
    }

    public OrientGraphNoTx getNoTxGraph() {
        return factory.getNoTx();
    }

    public OrientGraph getTxGraph() {
        return factory.getTx();
    }
}
