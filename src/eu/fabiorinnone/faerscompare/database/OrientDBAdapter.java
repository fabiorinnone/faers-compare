package eu.fabiorinnone.faerscompare.database;

import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import com.tinkerpop.blueprints.Vertex;
import eu.fabiorinnone.faerscompare.utilities.Utilities;
import eu.fabiorinnone.faerscompare.writer.OrientDBWriter;
import eu.fabiorinnone.faerscompare.writer.Writer;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by fabior on 02/02/17.
 */
public class OrientDBAdapter {

    private static OrientDBAdapter instance =  null;

    private OrientBaseGraph graph;

    private OrientDBAdapter(OrientBaseGraph graph) {
        this.graph = graph;
    }

    public static synchronized OrientDBAdapter getInstance(OrientGraphNoTx graphNoTx) {
        if (instance == null) {
            instance = new OrientDBAdapter(graphNoTx);
        }
        return instance;
    }

    public OrientBaseGraph getGraph() {
        return graph;
    }

    public void setGraph(OrientBaseGraph graph) {
        this.graph = graph;
    }

    public static synchronized OrientDBAdapter getInstance(OrientGraph graphTx) {
        if (instance == null) {
            instance = new OrientDBAdapter(graphTx);
        }
        return instance;
    }

    public void executeQuery(String query) throws IOException, SQLException {
        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();

        if (vertices != null) {
            OrientDBWriter writer = new OrientDBWriter(vertices);
            writer.write(Utilities.isFullyProjection(query));
        }
    }
}
